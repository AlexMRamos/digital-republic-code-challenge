``# DIGITAL REPUBLIC CODE CHALLENGE

Projeto para desafio da DIGITAL REPUBLIC

## Resumo

Uma aplicação em Java que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.

## Requisitos
- Java 11 ou superior

## Execução

Necessário rodar a classe DemoApplication.class

### Swagger

acessar o caminho http://localhost:8080/swagger-ui/index.html

acessar o caminho controller com seguinte payload

```
{
  "paredes": [
    {
      "altura": 0,
      "janela": 0,
      "largura": 0,
      "porta": 0
    }
  ]
}
```


### Plataforma de API (Postman/SoapUI)

Metodo POST
acessar localhost:8080/api/calculaTinta

via Body

```
{
  "paredes": [
    {
      "altura": 0,
      "janela": 0,
      "largura": 0,
      "porta": 0
    }
  ]
}
```

