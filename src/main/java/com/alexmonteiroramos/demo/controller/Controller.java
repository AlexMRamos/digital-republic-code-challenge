package com.alexmonteiroramos.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alexmonteiroramos.demo.handle.BusinessException;
import com.alexmonteiroramos.demo.model.dto.RequestTinta;
import com.alexmonteiroramos.demo.model.dto.ResponseTinta;
import com.alexmonteiroramos.demo.service.TintaService;

@RestController
@RequestMapping(value = "/api")
public class Controller {
	
	@Autowired
	private TintaService service;
	
	
	@PostMapping (value = "/calculaTinta")
	public ResponseEntity<ResponseTinta> calculoTinta(@RequestBody RequestTinta request) throws BusinessException{
			
	ResponseTinta response = service.calculaTinta(request);

	return ResponseEntity.ok().body(response);
	}

}
