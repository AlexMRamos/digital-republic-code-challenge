package com.alexmonteiroramos.demo.handle;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ExceptionConfig extends ResponseEntityExceptionHandler {

@ExceptionHandler({BusinessException.class })
public ResponseEntity<BasicErrorModel>  objectNotFoundExceptionHandler(BusinessException e, HttpServletRequest http) {
		
		BasicErrorModel error = BasicErrorModel
								.builder()
								.status(HttpStatus.UNPROCESSABLE_ENTITY.value())
								.message(e.getMessage())
								.path(http.getRequestURI().toString())
								.timestamp(new Date())
								.build();
		
		return ResponseEntity.unprocessableEntity().body(error);
	}

}

