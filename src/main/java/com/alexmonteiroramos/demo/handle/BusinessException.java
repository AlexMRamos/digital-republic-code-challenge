package com.alexmonteiroramos.demo.handle;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BusinessException extends RuntimeException {
	
	private static final long serialVersionUID = -3375037110612553973L;

	
	public BusinessException(String message) {
		super(message);
	}

}
