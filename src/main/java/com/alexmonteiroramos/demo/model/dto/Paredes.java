package com.alexmonteiroramos.demo.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Paredes {
	
	private Double altura;
	private Double largura;
	
	private int porta;
	private int janela;

}
