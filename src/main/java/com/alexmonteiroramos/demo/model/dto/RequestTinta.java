package com.alexmonteiroramos.demo.model.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RequestTinta {
	
	List<Paredes> paredes = new ArrayList<>();

}
