package com.alexmonteiroramos.demo.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponseTinta {
	
	//0,5L 2,5L, 3,6L, 18L
	
	private int Lata05L;
	private int Lata2L5;
	private int Lata3L6;
	private int Lata18L;
	private String litrosNecessario;

}
