package com.alexmonteiroramos.demo.model.enums;

import lombok.Getter;

@Getter
public enum Medidas {
	
	JANELA(1.2,2.0),
	PORTA(1.9, 0.8);
	
	
	private Double altura;
	private Double largura;


	Medidas(final Double altura, final Double largura) {
		 this.altura = altura;
		 this.largura = largura;
		
	}
	
	public static Double calculoAreaJanela() {
		return  JANELA.getLargura() * JANELA.getAltura();
	}
	
	public static Double calculoAreaPorta() {
		return  PORTA.getLargura() * PORTA.getAltura();
	}


	

}
