package com.alexmonteiroramos.demo.model.constant;

public class Latas {
	
	public static final double Tinta18L = 18.0;
	public static final double Tinta3L6 = 3.6;
	public static final double Tinta2L5 = 2.5;
	public static final double Tinta0L5 = 0.5;


}
