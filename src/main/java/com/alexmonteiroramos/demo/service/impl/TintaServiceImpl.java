package com.alexmonteiroramos.demo.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.alexmonteiroramos.demo.handle.BusinessException;
import com.alexmonteiroramos.demo.model.constant.Latas;
import com.alexmonteiroramos.demo.model.dto.Paredes;
import com.alexmonteiroramos.demo.model.dto.RequestTinta;
import com.alexmonteiroramos.demo.model.dto.ResponseTinta;
import com.alexmonteiroramos.demo.model.enums.Medidas;
import com.alexmonteiroramos.demo.service.TintaService;

@Service
public class TintaServiceImpl implements TintaService {

	private static final double PORCENTAGEM = 0.5;
	private static final double LIMITE = 0.30;
	private static final double MINIMO = 1.0;
	private static final double MAXIMO = 15.0;
	private static final double TINTA = 5.0;
	
	Double areaTotal = 0.0;

	@Override
	public ResponseTinta calculaTinta(RequestTinta request) throws BusinessException {
		
	
		validaTamanhoParede(request.getParedes());
			
		validaParedeComPortaJanela(request.getParedes());
		
		ResponseTinta response = QuantidadeTinta(areaTotal);	
		
		return response;
	}

	@Override
	public void validaTamanhoParede(List<Paredes> paredes) throws BusinessException {

		for (Paredes parede : paredes) {
			Double paredeTotal = parede.getLargura() * parede.getAltura();
			
			if (paredeTotal >= MINIMO && paredeTotal <= MAXIMO) {
				if(parede.getPorta() == 0) {
					continue;
				}
				else if(parede.getAltura() >= (Medidas.PORTA.getAltura() + LIMITE)) {
					continue;
				}else{
					throw new BusinessException("A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta.");
				}
			}
			else {
				throw new BusinessException("Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados.");
			}
		}

	}

	@Override
	public void validaParedeComPortaJanela(List<Paredes> paredes) throws BusinessException {

		for (Paredes parede : paredes) {

			Double paredeTotal = parede.getLargura() * parede.getAltura();

			if (parede.getPorta() != 0 && parede.getJanela() != 0) {
				if ((Medidas.calculoAreaJanela() * parede.getJanela()) + ((Medidas.calculoAreaPorta() * parede.getPorta())) <= paredeTotal * PORCENTAGEM) {
					areaTotal += paredeTotal - (Medidas.calculoAreaPorta() + Medidas.calculoAreaJanela());
				} else {
					throw new BusinessException("Porta e Janela deve ser no máximo 50% da area da parede");
				}
			} else if(parede.getPorta() > 0) {
				if (Medidas.calculoAreaPorta() * parede.getPorta() <= paredeTotal * PORCENTAGEM) {
					areaTotal += paredeTotal - Medidas.calculoAreaPorta();
				}else {
					throw new BusinessException("Porta deve ser no máximo 50% da area da parede");
					}
				} else if(parede.getJanela() > 0) {
				if (Medidas.calculoAreaJanela() * parede.getJanela() <= paredeTotal * PORCENTAGEM) {
					areaTotal += paredeTotal - Medidas.calculoAreaJanela();
				}
				else {
					throw new BusinessException("Janela deve ser no máximo 50% da area da parede");
				}
			} 


		}

	}
	
	
	private ResponseTinta QuantidadeTinta(Double areaTotal) {
		
		ResponseTinta response = new ResponseTinta();
		Double quantidadeTinta = areaTotal / TINTA; 
		
		response.setLitrosNecessario(quantidadeTinta.toString());
		
		while(quantidadeTinta > Latas.Tinta18L) {
			response.setLata18L((int) (quantidadeTinta/Latas.Tinta18L));
			quantidadeTinta %= Latas.Tinta18L;
		}
		while(quantidadeTinta > Latas.Tinta3L6) {
			response.setLata3L6((int) (quantidadeTinta/Latas.Tinta3L6));
			quantidadeTinta %= Latas.Tinta3L6;
		}
		
		while(quantidadeTinta > Latas.Tinta2L5) {
			response.setLata2L5((int) (quantidadeTinta/Latas.Tinta2L5));
			quantidadeTinta = quantidadeTinta%=Latas.Tinta2L5;
		}
		
		while(quantidadeTinta > 0.0) {
			quantidadeTinta -= Latas.Tinta0L5;
			response.setLata05L(response.getLata05L() + 1);
		}
		
		
		
		return response;
	}
}
