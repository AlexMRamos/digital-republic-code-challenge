package com.alexmonteiroramos.demo.service;

import java.util.List;

import com.alexmonteiroramos.demo.handle.BusinessException;
import com.alexmonteiroramos.demo.model.dto.Paredes;
import com.alexmonteiroramos.demo.model.dto.RequestTinta;
import com.alexmonteiroramos.demo.model.dto.ResponseTinta;

public interface TintaService {
	
	//calcular a quantidade de tinta necessária para pintar uma sala
	ResponseTinta calculaTinta(RequestTinta request) throws BusinessException;

	//Nenhuma parede pode ter menos de 1 metro quadrado
	//nem mais de 50 metros quadrados,
	//mas podem possuir alturas e larguras diferentes
	//A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
	void validaTamanhoParede(List<Paredes> paredes) throws BusinessException;
	
	
	//O total de área das portas e janelas deve ser no máximo 50% da área de parede
	void validaParedeComPortaJanela(List<Paredes> paredes) throws BusinessException;
}
